import { inject, NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { of, tap } from 'rxjs';
import { AuthGuard, canActivateGuard } from './core/auth/auth.guard';
import { AuthenticationService } from './core/auth/authentication.service';

const routes: Routes = [
  { path: 'demo1', loadChildren: () => import('./features/demo1/demo1.module').then(m => m.Demo1Module) },
  { path: 'demo2', loadChildren: () => import('./features/demo2/demo2.module').then(m => m.Demo2Module) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  {
    path: 'home',
    loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule),
    // class based - deprecated
    // canActivate: [AuthGuard]
    // function based
    // canActivate: [canActivateGuard]
    // function based with canMatch (no loading chunk)
    canActivate: [canActivateGuard]
  },
  { path: 'form-demo1', loadChildren: () => import('./features/form-demo1/form-demo1.module').then(m => m.FormDemo1Module) },
  { path: 'form-demo2', loadChildren: () => import('./features/form-demo2/form-demo2.module').then(m => m.FormDemo2Module) },
  { path: 'form-demo3', loadChildren: () => import('./features/form-demo3/form-demo3.module').then(m => m.FormDemo3Module) },
  { path: 'form-demo4', loadChildren: () => import('./features/form-demo4/form-demo4.module').then(m => m.FormDemo4Module) },
  { path: 'form-demo5', loadChildren: () => import('./features/form-demo5/form-demo5.module').then(m => m.FormDemo5Module) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
