import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { distinct, distinctUntilChanged, interval, Subject, Subscription, takeUntil, withLatestFrom } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Directive({
  selector: '[appIfLogged]',
  standalone: true,
})
export class IfLoggedDirective implements OnInit {
  @Input() appIfLogged: string | undefined
  subject = new Subject()

  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private authSrv: AuthenticationService
  ) {}

  ngOnInit() {
    this.authSrv.isLogged$
      .pipe(
        withLatestFrom(this.authSrv.role$),
        distinctUntilChanged(),
        takeUntil(this.subject)
      )
      .subscribe(([isLogged, loggedUserRole]) => {
        if (isLogged && loggedUserRole === this.appIfLogged) {
          this.view.createEmbeddedView(this.template)
        } else {
          this.view.clear();
        }
      })
  }

  ngOnDestroy() {
    this.subject.next(null);
    this.subject.complete();
    // this.sub.unsubscribe();
  }





}
