import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, of, share } from 'rxjs';
import { Auth } from '../../model/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  auth$ = new BehaviorSubject<Auth | null>(null)

  constructor(private http: HttpClient) {

    const localStorageData = window.localStorage.getItem('auth');
    if (localStorageData) {
      const auth = JSON.parse(localStorageData)
      this.auth$.next(auth)
    }
  }

  login(username: string, password: string) {
    const params = new HttpParams()
      .set('username', username)
      .set('password', password);

    const req = this.http.get<Auth>(`http://localhost:3000/login`, { params })
      .pipe(share())

    req.subscribe(auth => {
        this.auth$.next(auth);
        window.localStorage.setItem('auth', JSON.stringify(auth))
        // redirect
      })

    return req;
  }

  logout() {
    this.auth$.next(null);
    window.localStorage.removeItem('auth')
  }

  get isLogged$() {
    return this.auth$
      .pipe(
        map(data => !!data)
      )
  }

  get token$() {
    return this.auth$
      .pipe(
        map(data => data?.token)
      )
  }

  get role$() {
    return this.auth$
      .pipe(
        map(data => data?.role)
      )
  }

  get displayName$() {
    return this.auth$
      .pipe(
        map(data => data?.displayName)
      )
  }
}
