import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { first, iif, mergeMap, Observable, take, withLatestFrom } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthenticationService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.isLogged$
      .pipe(
        first(),
        withLatestFrom(this.authService.token$),
        mergeMap(([isLogged, token]) => {
          return iif(
            () => isLogged,
            next.handle(request.clone({ setHeaders: { Authorization: token!}})),
            next.handle(request)
          )
        })
      )
  }

  intercept3(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.isLogged$
      .pipe(
        first(),
        withLatestFrom(this.authService.token$),
        mergeMap(([isLogged, token]) => {
          let cloneReq = request;
          if (isLogged && token) {
            cloneReq = request.clone({
              setHeaders: {
                Authorization: token
              }
            })
          }
          return next.handle(cloneReq)
        })
      )
  }


  intercept2(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.token$
      .pipe(
        first(),
        mergeMap(tk => {
          let cloneReq = request;
          if (tk) {
            cloneReq = request.clone({
              setHeaders: {
                Authorization: tk
              }
            })
          }
          return next.handle(cloneReq)
        })
      )
  }


  intercept1(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const tk = this.authService.auth$.getValue()?.token;

    if (tk) {
      const cloneReq = request.clone({
        setHeaders: {
          Authorization: tk
        }
      })
      return next.handle(cloneReq);

    }
    return next.handle(request);

  }
}
