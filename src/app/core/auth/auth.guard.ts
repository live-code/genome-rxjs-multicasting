import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { map, mergeMap, Observable, of, tap } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({ providedIn: 'root'})
export class AuthGuard implements CanActivate{

  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {
  }
  canActivate(): Observable<boolean> {

    return this.authService.isLogged$
      .pipe(
        tap(isLogged => {
          if (!isLogged) {
            this.router.navigateByUrl('/login')
          }
        })
      )
  }
}

export function canActivateGuard() {
  const authSrv = inject(AuthenticationService);
  const router = inject(Router);
  const http = inject(HttpClient);
  console.log('qui')

  return authSrv.token$
    .pipe(
      mergeMap(tk => http.get<{response: string}>('http://localhost:3000/validateRoute?tk='+tk)
        .pipe(
          map(res => res.response === 'ok'),
          tap(valid => {
            if (!valid) {
              router.navigateByUrl('/login')
            }
          })
        ))
    )



}
export function canActivateGuardSync() {
  const authSrv = inject(AuthenticationService);
  const router = inject(Router);

  return authSrv.isLogged$
    .pipe(
      tap(isLogged => {
        if (!isLogged) {
          router.navigateByUrl('/login')
        }
      })
    )
}
