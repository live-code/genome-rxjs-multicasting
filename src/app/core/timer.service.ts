import { Injectable } from '@angular/core';
import { BehaviorSubject, interval, share, take } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  timer$ = new BehaviorSubject(123)
  _timer$ = interval(1000)
    .pipe(
      take(3)
    )

  constructor() {
    this._timer$.subscribe(this.timer$)
  }
}
