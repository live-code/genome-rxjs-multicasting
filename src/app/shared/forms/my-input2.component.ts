import { Component, Injector, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  FormsModule, NG_VALIDATORS,
  NG_VALUE_ACCESSOR, NgControl,
  ReactiveFormsModule, ValidationErrors, Validator,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-my-input2',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  template: `
    <div>
      <div *ngIf="ngControl.errors?.['required']">Field Required</div>
      <div *ngIf="ngControl.errors?.['minlength']">too short</div>
      <div *ngIf="ngControl.errors?.['alphaNumeric']">no alphanumerc</div>
      
      <label for="YourName">{{label}}</label>
      <input 
        type="text" class="form-control"
        [placeholder]="'Your ' + label"
        [formControl]="input"
        (blur)="onTouched()"
      >
    </div>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: MyInput2Component, multi: true},
    { provide: NG_VALIDATORS, useExisting: MyInput2Component, multi: true},
  ]
})
export class MyInput2Component implements ControlValueAccessor, Validator {
  @Input() label: string = ''
  @Input() alphaNumeric: boolean = false

  input = new FormControl('', { nonNullable: true})
  onTouched!: () => void;
  ngControl!: NgControl

  constructor(private inj: Injector) {}

  ngOnInit() {
    this.ngControl = this.inj.get(NgControl)
  }

  registerOnChange(fn: any): void {
    this.input.valueChanges.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(text: string): void {
    this.input.setValue(text)
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (
      this.alphaNumeric &&
      control.value
      && !control.value.match(ALPHA_NUMERIC_REGEX)
    ) {
      return { alphaNumeric: true}
    }
    return null;
  }

}

const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;
