import { CommonModule, NgForOf } from '@angular/common';
import { Component } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-my-rate',
  standalone: true,
  template: `
    <i
      *ngFor="let star of [null,null,null,null,null]; let i = index"
      class="fa"
      [ngClass]="{
        'fa-star': (i+1) <= value,
        'fa-star-o': (i+1) > value
      }"
      (click)="selectRate(i+1)"
    ></i>
    
  `,
  imports: [
    CommonModule
  ],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: MyRateComponent, multi: true}
  ]
})
export class MyRateComponent implements ControlValueAccessor{
  value!: number;

  onChange!: (rate: number) => void;
  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(rate: number): void {
    this.value = rate;
  }

  selectRate(value: number) {
    this.value = value
    this.onChange(value);
    this.onTouched()
  }
}
