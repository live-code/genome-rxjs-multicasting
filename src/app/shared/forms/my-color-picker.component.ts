import { CommonModule, NgForOf } from '@angular/common';
import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-my-color-picker',
  standalone: true,
  imports: [ CommonModule ],
  template: `
    <div class="d-flex">
      <div 
        class="cell"
        [ngClass]="{active: c === selectedColor}"
        *ngFor="let c of colors"
        [style.background-color]="c"
        (click)="changeColorHandler(c)"
        [style.opacity]="isDisabled ? 0.3 : 1"
        [style.pointer-events]="isDisabled ? 'none' : 'auto'"
      > </div>
    </div>
  `,
  styles: [`
    .cell {
      width: 30px; height: 30px;
    }
    .active {
      border: 2px solid black
    }
  `],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: MyColorPickerComponent, multi: true }
  ]
})
export class MyColorPickerComponent implements ControlValueAccessor{
  selectedColor: string = '';
  isDisabled: boolean = false;
  @Input() colors = ['#F44336', '#90CAF9', '#FFCDD2', '#69F0AE', '#AED581', '#FFE082']
  onChange!: (color: string) => void;
  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(color: string): void {
    this.selectedColor = color;
  }

  changeColorHandler(c: string) {
    // if (this.isDisabled) return;
    this.selectedColor = c;
    this.onChange(c)
    this.onTouched()
  }

  setDisabledState?(isDisabled: boolean) {
    this.isDisabled = isDisabled
  }

}
