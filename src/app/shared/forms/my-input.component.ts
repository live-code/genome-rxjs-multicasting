import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlValueAccessor, FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-my-input',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  template: `
    <div>
      <label for="YourName">{{label}}</label>
      <input 
        type="text" class="form-control"
        [placeholder]="'Your ' + label"
        [value]="value"
        (input)="onChangeHandler($event)"
        (blur)="onTouched()"
      >
    </div>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: MyInputComponent, multi: true}
  ]
})
export class MyInputComponent implements ControlValueAccessor {
  @Input() label: string = ''
  value = '';

  onChange!: (text: string) => void;
  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(text: string): void {
    this.value = text;
  }

  onChangeHandler(event: Event) {
    this.onChange((event.currentTarget as HTMLInputElement).value)
  }
}
