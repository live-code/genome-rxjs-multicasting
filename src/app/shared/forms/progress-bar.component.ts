import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-progress-bar',
  standalone: true,
  template: `
    <div class="bar"
         [style.width.%]="getPerc()"></div>

  `,
  styles: [`
    .bar {
      width: 100%;
      transition: width 2s cubic-bezier(0.68, -0.6, 0.32, 1.6);
      background-color: green;
      height: 4px;
    }
  `]
})
export class ProgressBarComponent {
  @Input() input!: FormControl;

  getPerc() {
    const minLengthError = this.input.errors?.['minlength'];
    if (minLengthError) {
      return (minLengthError['actualLength'] / minLengthError['requiredLength']) * 100
    }
    return 0
  }

}
