import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormDemo5Component } from './form-demo5.component';

const routes: Routes = [{ path: '', component: FormDemo5Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormDemo5RoutingModule { }
