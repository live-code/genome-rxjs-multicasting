import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormDemo5RoutingModule } from './form-demo5-routing.module';
import { FormDemo5Component } from './form-demo5.component';


@NgModule({
  declarations: [
    FormDemo5Component
  ],
  imports: [
    CommonModule,
    FormDemo5RoutingModule
  ]
})
export class FormDemo5Module { }
