import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  template: `
    <p>
      home works!
    </p>
  `,
  styles: [
  ]
})
export class HomeComponent {
  input = new FormControl('', { nonNullable: true })
  form = this.fb.nonNullable.group<{ name: string}>({
    name: ''
  })

  constructor(private fb: FormBuilder) {
    const a = this.input.value
    const b = this.form.value.name
    const c = this.form.getRawValue().name
  }
}
