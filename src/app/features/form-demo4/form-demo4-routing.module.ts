import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormDemo4Component } from './form-demo4.component';

const routes: Routes = [{ path: '', component: FormDemo4Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormDemo4RoutingModule { }
