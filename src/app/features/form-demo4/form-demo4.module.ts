import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormDemo4RoutingModule } from './form-demo4-routing.module';
import { FormDemo4Component } from './form-demo4.component';


@NgModule({
  declarations: [
    FormDemo4Component
  ],
  imports: [
    CommonModule,
    FormDemo4RoutingModule
  ]
})
export class FormDemo4Module { }
