import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IfLoggedDirective } from '../../core/auth/if-logged.directive';

import { Demo1RoutingModule } from './demo1-routing.module';
import { Demo1Component } from './demo1.component';


@NgModule({
  declarations: [
    Demo1Component,
  ],
  imports: [
    CommonModule,
    IfLoggedDirective,
    Demo1RoutingModule
  ]
})
export class Demo1Module { }
