import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { interval, share, shareReplay } from 'rxjs';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo1',
  template: `
    <div *ngIf="user$ | async as usr">
      {{usr.id}}
      {{usr.name}}
    </div>
    
    <form>...</form>
    
    <div>
      {{(user$ | async)?.name}}
    </div>
    
    <button *appIfLogged="'admin'">ADMIN ONLY</button>
  `,
})
export class Demo1Component {
  user$ = this.http.get<User>('https://jsonplaceholder.typicode.com/users/1')
    .pipe(shareReplay(1))

  constructor(private http: HttpClient) {

    this.user$
      .subscribe({
        next: res => {
          console.log(res)
        },
        error: err => {
          console.log('errore', err)
        },
        complete: () => {
          console.log('completed')
        }
      })

    setTimeout(() => {
      this.user$
        .subscribe({
          next: res => {
            console.log(res)
          },
          error: err => {
            console.log('errore', err)
          },
          complete: () => {
            console.log('completed')
          }
        })
    }, 2000)

  }
}
