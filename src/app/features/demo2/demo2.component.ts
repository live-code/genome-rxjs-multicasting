import { Component } from '@angular/core';
import { BehaviorSubject, map, ReplaySubject, Subject } from 'rxjs';
import { ThemeService } from '../../core/theme.service';
import { TimerService } from '../../core/timer.service';

@Component({
  selector: 'app-demo2',
  template: `
    <p>
      demo2 works! 
      {{timerSrv.timer$ | async}}
    </p>
    
    <!--<button (click)="getTimer()">
      {{localTimer || 'START'}}
    </button>-->
    <button (click)="themeService.changeTheme('light')">light</button>
    <button (click)="themeService.changeTheme('dark')">dark</button>
  `,
})
export class Demo2Component {
  localTimer: number | undefined;

  constructor(
    public timerSrv:TimerService,
    public themeService: ThemeService
  ) {
    this.timerSrv.timer$
      .subscribe({
        next: val => console.log(val),
        error: () => console.log('errore'),
        complete: () => console.log('completed')
      } )
  }

}
