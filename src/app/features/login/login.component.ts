import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { filter, withLatestFrom } from 'rxjs';
import { AuthenticationService } from '../../core/auth/authentication.service';

@Component({
  selector: 'app-login',
  template: `
    <h1>Login</h1>
    
    <form [formGroup]="form" (submit)="doLogin()">
      <input type="text" formControlName="username">
      <input type="text" formControlName="password">
      <button type="submit" [disabled]="form.invalid">LOGIN</button>
      
    </form>
    
    {{authenticationService.auth$ | async | json}}
  `,
})
export class LoginComponent {
  form = this.fb.nonNullable.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  })

  constructor(
    private fb: FormBuilder,
    public authenticationService: AuthenticationService,
    public router: Router
  ) {

    this.authenticationService
      .isLogged$
      .pipe(
        filter(isLogged => isLogged)
      )
      .subscribe(val => {
        //  this.router.navigateByUrl('/home')
      })

    this.form.valueChanges
      .pipe(
        withLatestFrom(this.authenticationService.token$)
      )
      .subscribe(([formData, tk]) => {
        console.log(formData, tk)
      })
  }


  doLogin() {
    this.authenticationService.login(
      this.form.value.username!,
      this.form.value.password!,
    )
     /* .subscribe(
        val => window.alert('redirect')
      )*/

  }
}
