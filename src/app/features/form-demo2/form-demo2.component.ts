import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, ValidationErrors, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-demo2',
  template: `
    <h1>Form 2</h1>

    <form [formGroup]="form" (submit)="save()">
      <input type="checkbox" formControlName="isCompany"> is Company?

      <div>
        {{form.get('name')?.errors | json}}
        <label for="YourName">Your name</label>
        <input 
          type="text"
          placeholder="Your name"
          formControlName="name" class="form-control">
      </div>

      <!--
       <app-my-input
         formControlName="surname"
         label="Surname"></app-my-input>
       -->
      
      <app-my-input2
        [alphaNumeric]="true"
        formControlName="surname"
        label="Surname"></app-my-input2>

      <br>
      <br>
      <input
        type="text" formControlName="piCF"
        [ngClass]="{'is-invalid': form.get('piCF')?.invalid}"
        [placeholder]="form.get('isCompany')?.value ? 'P.iva' : 'Cod.Fisc'" class="form-control">

      <app-my-color-picker
        [colors]="['red', 'yellow', 'brown']"
        formControlName="bg"
      ></app-my-color-picker>

      <app-my-rate
        formControlName="rate"
      ></app-my-rate>
      <button type="submit" [disabled]="form.invalid">save</button>
    </form>

    <pre>dirty: {{form.dirty | json}}</pre>
    <pre>touched: {{form.touched | json}}</pre>
    <pre>valid: {{form.get('surname')?.valid | json}}</pre>
    <pre>value: {{form.value | json}}</pre>

  `,
})
export class FormDemo2Component {
  form = this.fb.nonNullable.group({
    name: ['Cic', [matchName('Ciccio')]],
    surname: ['bio', [Validators.required, Validators.minLength(3)]],
    isCompany: true,
    piCF: ['', [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
    bg: ['', [Validators.required]],
    rate: [0, Validators.min(1)]
  })
  constructor(private fb: FormBuilder) {
   setTimeout(() => {
      // this.form.get('surname')?.setValue('ciccio')
      // this.form.get('bg')?.setValue('purple')
     //  this.form.get('bg')?.disable()
      // this.form.patchValue({ bg: 'yellow' })
    }, 2000)


    this.form.get('isCompany')?.valueChanges
      .subscribe(isCompany => {
        if (isCompany) {
          this.form.get('piCF')?.setValidators([Validators.minLength(11), Validators.maxLength(11)])
        } else {
          this.form.get('piCF')?.setValidators([Validators.minLength(16), Validators.maxLength(16)])
        }
        this.form.get('piCF')?.updateValueAndValidity()
      })

  }

  save() {
    console.log(this.form.value)
  }
}


function matchName(nameToCheck: string): ValidationErrors | null {
  return (control: AbstractControl) => {
    if (control.value !== nameToCheck) {
      return { noMatch: true}
    }
    return null;
  }

}
