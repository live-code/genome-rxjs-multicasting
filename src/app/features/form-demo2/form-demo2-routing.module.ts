import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormDemo2Component } from './form-demo2.component';

const routes: Routes = [{ path: '', component: FormDemo2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormDemo2RoutingModule { }
