import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MyColorPickerComponent } from '../../shared/forms/my-color-picker.component';
import { MyInputComponent } from '../../shared/forms/my-input.component';
import { MyInput2Component } from '../../shared/forms/my-input2.component';
import { MyRateComponent } from '../../shared/forms/my-rate.component';

import { FormDemo2RoutingModule } from './form-demo2-routing.module';
import { FormDemo2Component } from './form-demo2.component';


@NgModule({
  declarations: [
    FormDemo2Component
  ],
  imports: [
    MyColorPickerComponent,
    MyRateComponent,
    CommonModule,
    FormDemo2RoutingModule,
    ReactiveFormsModule,
    MyInputComponent,
    MyInput2Component
  ]
})
export class FormDemo2Module { }
