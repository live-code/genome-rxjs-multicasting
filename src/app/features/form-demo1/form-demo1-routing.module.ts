import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormDemo1Component } from './form-demo1.component';

const routes: Routes = [{ path: '', component: FormDemo1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormDemo1RoutingModule { }
