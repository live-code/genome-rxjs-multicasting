import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

const REGEX_URL = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/

@Component({
  selector: 'app-form-demo1',
  template: `
    <h1>Typed Form</h1>
    
    <div style="width: 200px">
      <input type="text" [formControl]="myInput" style="width: 100%">
      <app-progress-bar [input]="myInput"></app-progress-bar>
    </div>
    
    <div class="errorPanel" *ngIf="myInput.errors as err">
      <div *ngIf="err['required']">field required</div>
      <div *ngIf="err['minlength'] as min">
        field too short. Missing {{min['requiredLength'] - min['actualLength']}} chars
      </div>
    </div>
    
    <input type="text" >
    <input type="text" >
    
    
    <hr>
    {{myInput.errors | json}}
  `,
  styles: [`
    .errorPanel {
      background-color: palevioletred; padding: 10px
    }
  `]
})
export class FormDemo1Component {

  myInput = new FormControl('', {
    nonNullable: true,
    validators: [Validators.required, Validators.minLength(3)]
  });

  myInput2 = new FormControl('', {
    nonNullable: true,
    validators: [Validators.required, Validators.pattern(REGEX_URL)]
  });

  // myInput2 = new FormControl('', [Validators.required, Validators.minLength(3)]);

  constructor() {

    setTimeout(() => {
    }, 2000)
  }

}
