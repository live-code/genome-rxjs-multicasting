import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ProgressBarComponent } from '../../shared/forms/progress-bar.component';

import { FormDemo1RoutingModule } from './form-demo1-routing.module';
import { FormDemo1Component } from './form-demo1.component';


@NgModule({
  declarations: [
    FormDemo1Component
  ],
  imports: [
    CommonModule,
    ProgressBarComponent,
    FormDemo1RoutingModule,
    ReactiveFormsModule
  ]
})
export class FormDemo1Module { }
