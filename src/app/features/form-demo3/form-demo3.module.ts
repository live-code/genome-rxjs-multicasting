import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormDemo3RoutingModule } from './form-demo3-routing.module';
import { FormDemo3Component } from './form-demo3.component';


@NgModule({
  declarations: [
    FormDemo3Component
  ],
  imports: [
    CommonModule,
    FormDemo3RoutingModule
  ]
})
export class FormDemo3Module { }
