import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormDemo3Component } from './form-demo3.component';

const routes: Routes = [{ path: '', component: FormDemo3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormDemo3RoutingModule { }
