import { Component } from '@angular/core';
import { AuthenticationService } from './core/auth/authentication.service';
import { ThemeService } from './core/theme.service';

@Component({
  selector: 'app-root',
  template: `
    <div>
      <button routerLink="demo1">demo1</button>
      <button routerLink="demo2">demo2</button>
      <button routerLink="login">login</button>
      <button
        routerLink="home"
        *appIfLogged="'admin'"
      >home</button>
      <button
        *ngIf="authenticationService.isLogged$ | async"
        (click)="authenticationService.logout()"
      >logout</button>

      <button routerLink="form-demo1">form 1</button>
      <button routerLink="form-demo2">form 2</button>
      <button routerLink="form-demo3">form 3</button>
      <button routerLink="form-demo4">form 4</button>
      <button routerLink="form-demo5">form 5</button>
      
      {{authenticationService.displayName$ | async | json}}
      {{authenticationService.role$ | async}}
    </div>
    
    
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {

  constructor(
    public themeService: ThemeService,
    public authenticationService: AuthenticationService
  ) {
  }
}
